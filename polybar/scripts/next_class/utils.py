"""Helper functions.

TODO:
- Put this into a class
"""


import datetime


def time_diff(a: datetime.time, b: datetime.time) -> str:
    """Return a human readable time difference between 2 times.

    A happened before B.
    Assumption: They both took place in the same day (x: 3:24 and 6:12)
    """
    # Find the difference between the times in seconds.
    a_secs = a.hour * 60 * 60 + a.minute * 60 + a.second
    b_secs = b.hour * 60 * 60 + b.minute * 60 + b.second
    overflow_secs = b_secs - a_secs
    
    # Calculate the ~~hours and~~ minutes.
    overflow_mins, secs = divmod(overflow_secs, 60)
    # hours, mins = divmod(overflow_mins, 60)

    # Round the minutes. #, then round the hours.
    # # Since all classes are < 2 hours long, show minutes.
    # X: 153 minutes
    rounded_overflow_mins = overflow_mins if secs < 30 else overflow_mins + 1
    
    return str(rounded_overflow_mins)

def display(curr_class: tuple, next_class: tuple, now: datetime.datetime) -> str:
    """Return the string for the status bar.

    Before school: 'Before school.\\nScience in 75.'
    Classes: 'Math ends in 10 minutes.\\nHistory in 15.'
    Breaks: 'Break.\\nHistory in 4.'
    After school:  'After school.\\n'

    curr_class:
        tuple
        str: Before school | After school | Break
    next_class:
        tuple
        None
    """
    # Make the sentence for the current class.
    if curr_class in ('Before school', 'After school', 'Break'):
        curr_sentence = curr_class + '.'
    else:
        curr_name = curr_class[0]
        time_until_class_ends = time_diff(now, curr_class[2])
        curr_sentence = f'{curr_name} for {time_until_class_ends}.'

    # Make the sentence for the next class.
    if next_class:
        next_name = next_class[0]
        time_until_next_class = time_diff(now, next_class[1])
        next_sentence = f'{next_name} in {time_until_next_class}.'
    else:
        next_sentence = ''

    # Put the sentences together.
    spacing = ' ' if curr_sentence and next_sentence else ''
    return curr_sentence + spacing + next_sentence

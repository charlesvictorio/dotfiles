"""Configuration for this project.
"""


import datetime

# Semester 1

# SHED = {
#         # name, start, end
#         # [start, end) (?)
#     0: [
#         ['Cross Country', datetime.time(8), datetime.time(9)], # Check in
#         ['Science', datetime.time(9), datetime.time(10)], # Check in
#         ['Math', datetime.time(10), datetime.time(11)], # Check in
#         ['English', datetime.time(11), datetime.time(12)], # Check in
#         ['History', datetime.time(12), datetime.time(13)], # Check in
#         ['Advisement', datetime.time(13, 30), datetime.time(14)], # Class
#         ['Spanish', datetime.time(14), datetime.time(15)] # Check in
#     ],
#     1: [
#         ['Science', datetime.time(8, 30), datetime.time(9, 55)], # Class
#         ['Math', datetime.time(10, 10), datetime.time(11, 35)], # Check in
#         ['History', datetime.time(11, 45), datetime.time(13, 10)], # Class
#         # ['Cross Country', datetime.time(13, 45), datetime.time(15, 10)] # Check in
#     ],
#     2: [
#         ['Science', datetime.time(8, 30), datetime.time(9, 55)], # Check in
#         ['Math', datetime.time(10, 10), datetime.time(11, 35)], # Class
#         ['History', datetime.time(11, 45), datetime.time(13, 10)] # Check in
#     ],
#     3: [
#         ['English', datetime.time(10, 10), datetime.time(11, 35)], # Check in
#         ['Spanish', datetime.time(11, 45), datetime.time(13, 10)] # Class
#     ],
#     4: [
#         ['English', datetime.time(10, 10), datetime.time(11, 35)], # Class
#         ['Spanish', datetime.time(11, 45), datetime.time(13, 10)], # Check in
#         # ['Cross Country', datetime.time(13, 45), datetime.time(15, 10)] # Check in
#     ],
#     5: [
#         ['No school', datetime.time.min, datetime.time.max]
#     ],
#     6: [
#         ['No school', datetime.time.min, datetime.time.max]
#     ]
# }

# Semester 2
# ?: Break for odd|even classes system.

SHED = {
        # name, start, end
        # [start, end) (?)
    0: [
        ['Cross Country', datetime.time(8), datetime.time(9)], # Optional
        ['Science', datetime.time(9), datetime.time(10)], # Optional
        ['Math', datetime.time(10), datetime.time(11)], # Optional
        ['English', datetime.time(11), datetime.time(12)], # Optional
        ['History', datetime.time(12), datetime.time(13)], # Optional
        ['Homeroom', datetime.time(13, 30), datetime.time(14)],
        ['Spanish', datetime.time(14), datetime.time(15)] # Optional
    ],
    1: [
        ['Math', datetime.time(8, 30), datetime.time(9, 55)], # Class
        ['Spanish', datetime.time(10, 10), datetime.time(11, 35)], # Check in
        ['English', datetime.time(11, 45), datetime.time(13, 10)], # Class
    ],
    2: [
        ['Math', datetime.time(8, 30), datetime.time(9, 55)], # Check in
        ['Spanish', datetime.time(10, 10), datetime.time(11, 35)], # Class
        ['English', datetime.time(11, 45), datetime.time(13, 10)] # Check in
    ],
    3: [
        ['Science', datetime.time(10, 10), datetime.time(11, 35)], # Check in
        ['History', datetime.time(11, 45), datetime.time(13, 10)] # Class
    ],
    4: [
        ['Science', datetime.time(10, 10), datetime.time(11, 35)], # Class
        ['History', datetime.time(11, 45), datetime.time(13, 10)], # Check in
        # ['Cross Country', datetime.time(13, 45), datetime.time(15, 10)] # Check in
    ],
    5: [
        ['No school', datetime.time.min, datetime.time.max]
    ],
    6: [
        ['No school', datetime.time.min, datetime.time.max]
    ]
}

#!/usr/bin/env python

"""Show time of the current class and the next class.

TODO:
- Half open? Test boundaries (for monday)
- classes should be named tuples
- learn typing module
"""

import datetime

import config
import utils


def get_classes_display_now() -> str:
    """Get the display string of the current and next class."""
    now = datetime.datetime.now()
    classes = get_curr_and_next_classes_at_datetime(now)
    display_sentences = utils.display(*classes, now)
    return display_sentences

def get_curr_and_next_classes_at_datetime(dt: datetime.datetime) -> tuple or str:
    """Get the current and next class at a time."""
    schedule_for_day = config.SHED[dt.weekday()]
    time = dt.time()

    # Before school is before the start of the first class.
    if time < schedule_for_day[0][1]:
        return ('Before school', schedule_for_day[0])
    
    # After school is after the end of the last class.
    if time > schedule_for_day[-1][2]:
        return ('After school', None)

    # Go through each class.
    for i, curr_class in enumerate(schedule_for_day):
        # Get the current class (`curr_class`).

        # Get the next class.
        # If this class is the very last class,
        if i == len(schedule_for_day) - 1:
            # then there is no next class.
            next_class = None
        else:
            next_class = schedule_for_day[i + 1]

        # Sometimes, I am in between classes.
        # This means that the time is after the previous class, but before this one.
        if time < curr_class[1]:
            # So, the current class is actually 'Break', and
            # `curr_class` is actually the next class.
            next_class = curr_class
            # Break starts when the previous class ends.
            return ('Break', next_class)

        # Otherwise, the time is somewhere between the start and end of a class.
        if curr_class[1] <= time <= curr_class[2]:
            return (curr_class, next_class)

        # If the time is after the end of this class,
        # then move on to the next one.
        # 'After school' is the last class, so the execution will stop
        # before it leaves the for loop and reaches here.

if __name__ == '__main__':
    print(get_classes_display_now())

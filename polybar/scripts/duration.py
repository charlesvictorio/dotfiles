#!/usr/bin/env python

"""Script to return the days completed until the end.

X: "164 / 284 (58%)"
"""


import datetime


# 1. Get the date of the start, the end, and today.
start_date = datetime.date(2020, 8, 24)
end_date = datetime.date(2021, 6, 4)
today = datetime.date.today()

# 2. Get the days from start to end, the days from start to today, and the percentage completed so far.
total_time = (end_date - start_date).days
so_far_time = (today - start_date).days
percent = round((so_far_time / total_time) * 100)

# 3. Format and print the message.
message = f'{so_far_time:3} / {total_time:3} ({percent:2}%)'
print(message)

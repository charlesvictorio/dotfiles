#!/bin/sh

rm -rf alacritty i3 polybar zathura custom_shell_functions 

cp -r ~/.config/alacritty .
cp -r ~/.config/i3 .
cp -r ~/.config/polybar .
cp -r ~/.config/zathura .

cp ~/.config/zsh/custom_functions custom_shell_functions

# Dotfiles

Dotfiles for my computer.

```
- dotfiles: put on gitlab (resume, backup)
-- [x] polybar (~/.config/polybar)
-- [x] i3 (~/.config/i3)
-- [x] alacritty (1 config file) (~/.config/alacritty)
-- [x] zathura (1 config file) (~/.config/zathura)
-- [x] custom shell aliases and functions (~/.config/zsh/custom_functions)
# -- vim (~/.vimrc/, ~/.vim/) (standerdize plugins, list important ones, , )
# -- sublime
```

| name | description | original filepath |
| ---- | ----------- | ----------------- |
| alacritty | Terminal | ~/.config/alacritty |
| i3 | Window manager | ~/.config/i3 |
| polybar | Status bar | ~/.config/polybar |
| zathura | Pdf viewer | ~/.config/zathura |
| custom_shell_functions | ~/.config/zsh/custom_functions |
